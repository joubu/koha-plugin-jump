# Koha Plugin - Jump

This plugin opens unlimited possibilities for Koha developers.

## What's this?

It started with a simple idea then it got complex, as always. So far the UI, the design and the code are ugly.

The first idea was to add a simple method to jump from one page to another, then came the idea to generate data on demand, then data that matches given conditions, then etc.

What's possible:

  Jump to the following entities: biblio, item, itemtype, libraries, list (shelves)

  Create one of those entities, with or without attributes of your choices

Example:

  `jump biblio The art of` will display the list of bibliographic records matching "The art of" in their title, when selected you will be redirected to the selected record

  `create item` will create a new item

  `create 10 item biblionumber=20` will create 10 items attached to the bibliographic record with the biblionnumber=20

## How to use it

  Press `Alt+j` to display the modal window (press again Alt+j to hide it)

  Start typing a verb (like `jump` or `insert`)

  Select an object class (like `patron`, `item`, etc.)

  Search for one object starting entering a term, or tell the attributes you want to use to create the object

  Press `Alt+enter` will submit (click `Go`)


## About Koha plugins

Koha’s Plugin System (available in Koha 3.12+) allows for you to add additional tools and reports to [Koha](http://koha-community.org) that are specific to your library. Plugins are installed by uploading KPZ ( Koha Plugin Zip ) packages. A KPZ file is just a zip file containing the perl files, template files, and any other files necessary to make the plugin work. Learn more about the Koha Plugin System in the [Koha 3.22 Manual](http://manual.koha-community.org/3.22/en/pluginsystem.html) or watch [Kyle’s tutorial video](http://bywatersolutions.com/2013/01/23/koha-plugin-system-coming-soon/).

## Downloading

From the [release page](https://gitlab.com/joubu/koha-plugin-jump/-/releases) you can download the relevant *.kpz file

## Installing

The plugin system needs to be turned on by a system administrator.

To set up the Koha plugin system you must first make some changes to your install.

* Change `<enable_plugins>0<enable_plugins>` to `<enable_plugins>1</enable_plugins>` in your koha-conf.xml file
* Confirm that the path to `<pluginsdir>` exists, is correct, and is writable by the web server
* Restart your webserver
* Install the .kpz file from the Koha interface
* Restart plack

