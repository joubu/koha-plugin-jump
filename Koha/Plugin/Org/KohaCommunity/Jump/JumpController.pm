package Koha::Plugin::Org::KohaCommunity::Jump::JumpController;

# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

use Modern::Perl;

use Mojo::Base 'Mojolicious::Controller';
use File::Slurp;
use JSON;

use Koha::Patrons;
use Koha::Libraries;
use t::lib::TestBuilder;

=head1 API

=head2 Class Methods

=head3 Method that bothers the patron, with no side effects

=cut

use Koha::Plugin::Org::KohaCommunity::Jump;
sub verbs {
    my $c = shift->openapi->valid_input or return;
    my $term = $c->validation->param('term');

    my $bundle_path = Koha::Plugin::Org::KohaCommunity::Jump->new->bundle_path;
    my $file = read_file($bundle_path . '/jump.json');

    my $json = decode_json($file);
    my $verbs = $term ? [ grep { /$term/ } @{ $json->{verbs} } ] : $json->{verbs};
    $verbs = [ map { { value => $_, label => $_ } } @$verbs ];
    return $c->render( status => 200, openapi => $verbs );
}
sub object_classes {
    my $c = shift->openapi->valid_input or return;
    my $term = $c->validation->param('term');

    my $bundle_path = Koha::Plugin::Org::KohaCommunity::Jump->new->bundle_path;
    my $file = read_file($bundle_path . '/jump.json');

    my $json = decode_json($file);
    my $object_classes = $term ? [ grep { /$term/ } @{ $json->{object_classes} } ] : $json->{object_classes};
    $object_classes = [ map { { value => $_, label => $_ } } @$object_classes ];
    return $c->render( status => 200, openapi => $object_classes );
}

sub objects {
    my $c = shift->openapi->valid_input or return;
    my $object_class = $c->validation->param('object_class');
    my $term = $c->validation->param('term');

    my $bundle_path = Koha::Plugin::Org::KohaCommunity::Jump->new->bundle_path;
    my $file = read_file($bundle_path . '/jump.json');

    my $json = decode_json($file);
    my $koha_objects_class = $json->{objects}->{$object_class}->{koha_objects_class};
    my @search_fields = @{ $json->{objects}->{$object_class}->{search_fields} };
    my $conditions = { -or => {map { $_ => { -like => "%$term%"} } @search_fields} };
    my @to_string = @{$json->{objects}->{$object_class}->{to_string}};
    my @url = @{$json->{objects}->{$object_class}->{url}};
    my @objects = $koha_objects_class->search($conditions, { limit => 10 });
    @objects = map {
        my $o   = $_;
        my $lib = sprintf(
            $to_string[0],
            map { $o->$_ } @to_string[ 1 .. $#to_string ]
        );
        {
            value => sprintf( $url[0], map { $o->$_ } @url[ 1 .. $#url ] ),
            label => $lib
        }
    } @objects;
    return $c->render( status => 200, openapi => \@objects );
}

sub attributes {
    my $c = shift->openapi->valid_input or return;
    my $object_class = $c->validation->param('object_class');
    my $term = $c->validation->param('term');

    my $bundle_path = Koha::Plugin::Org::KohaCommunity::Jump->new->bundle_path;
    my $file = read_file($bundle_path . '/jump.json');

    my $json = decode_json($file);
    my $koha_objects_class = $json->{objects}->{$object_class}->{koha_objects_class};
    my $koha_object_class = $koha_objects_class->object_class;
    my $attributes = $koha_object_class->new->_columns;
    $attributes = $term ? [ grep { /$term/ } @$attributes ] : $attributes;
    return $c->render( status => 200, openapi => $attributes );
}

sub build {
    my $c = shift->openapi->valid_input or return;
    my $body = $c->validation->param('body');
    my $object_class = $body->{object_class};
    my $attributes = $body->{attributes} || [];
    my $builder = t::lib::TestBuilder->new;

    my $bundle_path = Koha::Plugin::Org::KohaCommunity::Jump->new->bundle_path;
    my $file = read_file($bundle_path . '/jump.json');

    my $json = decode_json($file);
    my $koha_objects_class = $json->{objects}->{$object_class}->{koha_objects_class};

    my $values;
    for my $attr (@$attributes) {
        my ( $var, $val ) = split ':', $attr;
        $values->{$var} = $val;
    }
    my $object = $builder->build_object({ class => $koha_objects_class, value => $values });
    my @to_string = @{$json->{objects}->{$object_class}->{to_string}};
    my @url = @{$json->{objects}->{$object_class}->{url}};
    my $display = sprintf(q|<a href="%s">%s</a>|,
        sprintf( $url[0], map { $object->$_ } @url[ 1 .. $#url ] ),
        sprintf( $to_string[0], map { $object->$_ } @to_string[ 1 .. $#to_string ] ),
    );

    return $c->render(status => 200, openapi => $display );
}

1;
