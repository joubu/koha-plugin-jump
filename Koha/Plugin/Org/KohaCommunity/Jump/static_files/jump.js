    $(document).ready(function(){
        var modal = $(''
            + '<div class="modal" id="dev_helper_modal" tabindex="-1" role="dialog" aria-labelledby="dev_helper_modal_label" aria-hidden="true">'
            + ' <div class="modal-dialog modal-lg">'
            + '  <div class="modal-content">'
            + '   <div class="modal-header">'
            + '    <button type="button" class="closebtn" data-dismiss="modal" aria-hidden="true">×</button>'
            + '    <h3 id="dev_helper_modal_label">The header</h3>'
            + '   </div>'
            + '   <div class="modal-body">'
            + '    <p>What todo?</p>'
            + '    <div id="sentence">'
            + '      <input type="text" id="verb" disabled="disabled" />'
            + '      <input type="text" id="quantity" disabled="disabled" />'
            + '      <input type="text" id="object_class" disabled="disabled" />'
            + '      <input type="text" name="attribute" />'
            + '    </div>'
            + '    <input type="text" id="jump_autocomplete" />'
            + '    <button class="btn btn-default" id="do">Do</button>'
            + '    <div>'
            + '     <ul id="result">'
            + '     </ul>'
            + '    </div>'
            + '   </div>'
            + '   <div class="modal-footer">'
            + '    <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>'
            + '   </div>'
            + '  </div>'
            + ' </div>'
            + '</div>');
        $(modal).appendTo($('body > div:first'));


        function get_current_state (){
            return {
                has_verb: $("#verb").val().length,
                has_quantity: $("#quantity").val().length,
                has_object_class: $("#object_class").val().length
            };
        }
        var state;
        function autocomplete(request , response) {
            state = get_current_state();
            if ( !state['has_verb'] ) {
                $.ajax({
                    url: '/api/v1/contrib/jump/verbs',
                    data: { term: request.term },
                    dataType: "json",
                    type: "GET",
                    success: function (data) {
                        response(data);
                    },
                    error: function (data) {
                        alert(data);
                    }
                });
            } else if ( !state['has_object_class'] ) {
                if ( !state['has_quantity'] ) {
                    if ($.isNumeric(request.term)) {
                        return response([request.term]);
                    }
                }
                $.ajax({
                    url: '/api/v1/contrib/jump/object_classes',
                    data: { term: request.term },
                    dataType: "json",
                    type: "GET",
                    success: function (data) {
                        response(data);
                    },
                    error: function (data) {
                        alert(data);
                    }
                });
            } else if ( $("#verb").val() == "jump" ) {
                $.ajax({
                    url: '/api/v1/contrib/jump/objects',
                    data: { term: request.term, object_class: $("#object_class").val() },
                    dataType: "json",
                    type: "GET",
                    success: function (data) {
                        response(data);
                    },
                    error: function (data) {
                        alert(data);
                    }
                });
            } else {
                var var_val = request.term.split('=');
                if( var_val.length == 2 ) {
                    return response([request.term]);
                }

                $.ajax({
                    url: '/api/v1/contrib/jump/attributes',
                    data: { term: request.term, object_class: $("#object_class").val() },
                    dataType: "json",
                    type: "GET",
                    success: function (data) {
                        response(data);
                    },
                    error: function (data) {
                        alert(data);
                    }
                });

                $("#do").show();
            }
            response([]);
        }

        var current_options = [];
        var map_label_value;

        function autoselect(value) {
            state = get_current_state();
            if ( !state['has_verb'] ) {
                var verb = value;
                $("#verb").val(verb).attr('size', $("#verb").val().length).show();
            } else if ( !state['has_quantity'] && $.isNumeric(value) ) {
                $("#quantity").val(value).attr('size', $("#quantity").val().length).show();
            } else if ( !state['has_object_class'] ) {
                var object_class = value;
                $("#object_class").val(object_class).attr('size', $("#object_class").val().length).show();
            } else if ( $("#verb").val() == 'jump' ) {
                var object_label = value;
                window.location = map_label_value[object_label];
            } else {
                var var_val = value.split('=');
                if( var_val.length == 2 ) {
                    var attribute_node = $('<input type="text" name="attribute" disabled="disabled"/>');
                    $(attribute_node).val(var_val[0]+':'+var_val[1]).attr('size', $(attribute_node).val().length);
                    $(attribute_node).appendTo($("#sentence"));
                }
            }
            $("#jump_autocomplete").val('');
            $("#jump_autocomplete").focus();
        }

        $("#dev_helper_modal").find('#jump_autocomplete').autocomplete({
            source: autocomplete,
            delay: 0,
            minLength: 0,
            autoFocus: true,
            select: function(event, selected){
                autoselect(selected.item.value)
                // FIXME update state here

                $(this).autocomplete('search', ''); // FIXME does not work we want to display all the options
                return false;
            },
            create: function() {
                $(this).data('ui-autocomplete')._renderItem  = function (ul, item) {
                    current_options.push(item['value']);
                    return $( "<li></li>" )
                    .data( "ui-autocomplete-item", item['label'] )
                    .append( item['label'] )
                    .appendTo( ul );
                };
                $(this).data('ui-autocomplete')._renderItemData = function( ul, item ) {
                    return this._renderItem( ul, item ).data( "ui-autocomplete-item", item );
                };
                $(this).data('ui-autocomplete')._renderMenu = function( ul, items ) {
                  current_options = [];
                  map_label_value = {};
                  var that = this;
                  $.each( items, function( index, item ) {
                    map_label_value[item['label']] = item['value'];
                    that._renderItemData( ul, { value: item['label'], label: item['label']});
                  });
                  $( ul ).find( "li:odd" ).addClass( "odd" );
                };
            }
        })
        .data( "ui-autocomplete" );

        $("#jump_autocomplete").keydown(function (e) {
            if (e.which == 32) { //space bar
                var value = $(this).val();
                if( $.inArray(value, current_options) !== -1 ) {
                    autoselect(value);
                }
                e.preventDefault()
            } else if (e.which == 8 && $(this).val() == "") { //backspace and input was empty
                $("#sentence > input:visible:last").val("").hide();
                $("#jump_autocomplete").autocomplete( "close" );
                $("#do").hide(); // Maybe not the better place to put this
                e.preventDefault()
            } else if (e.which == 9) { // Tab
                $("jump_autocomplete").find('li')
                e.preventDefault();
            } else if (e.which == 13) { // Enter
                //if ( $("#jump_autocomplete").val().length == 0 && $("#do").is(":visible") ) {
                //    // We want to save a click, but will certainly annoy us later.
                //    $("#do").click();
                //    e.preventDefault();
                //}
            }
        })

        $("#sentence > input").hide();

        $("#do").on("click", function(){
            var verb = $("#verb").val();
            if ( verb == 'insert' ) {
                var quantity = $("#quantity").val() || 1;
                var object_class = $("#object_class").val();
                var attributes = [];
                $('input[name="attribute"]').each(function(){
                    if ( $(this).val().length > 0 ) {
                        attributes.push($(this).val());
                    }
                });
                var data = { "object_class": object_class, "attributes": attributes};
                for ( var i = 1 ; i <= quantity ; i++ ) {
                    $.ajax({
                        url: '/api/v1/contrib/jump/build',
                        data: JSON.stringify(data),
                        dataType: "json",
                        type: "POST",
                        success: function (data) {
                            var result_node = $("#result");
                            var new_node = $("<li>"+data+"</li>");
                            $(result_node).append(new_node);
                        },
                        error: function (data) {
                            alert(data);
                        }
                    });
                }
            } else {
                alert ("Cannot do something for verb " + verb);
            }
        }).hide();

        shortcut.add('Alt+j',function (){
            $("#dev_helper_modal").toggle();
            $("#dev_helper_modal").find("#jump_autocomplete").focus();
        });
        shortcut.add('Alt+enter',function (){
            $("#do").click();
        });

    });
