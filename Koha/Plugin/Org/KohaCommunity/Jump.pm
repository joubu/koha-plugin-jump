package Koha::Plugin::Org::KohaCommunity::Jump;

use Modern::Perl;

use base qw(Koha::Plugins::Base);

use C4::Context;
use C4::Auth;
use Koha::Patron;
use Koha::DateUtils;
use Koha::Libraries;
use Koha::Patron::Categories;
use Koha::Account;
use Koha::Account::Lines;
use MARC::Record;
use Mojo::JSON qw(decode_json);;
use URI::Escape qw(uri_unescape);
use LWP::UserAgent;

our $VERSION = "v0.0.1";

our $metadata = {
    name            => 'Koha Developer Jump Plugin',
    author          => 'Jonathan Druart',
    date_authored   => '2019-12-20',
    date_updated    => "2019-12-20",
    minimum_version => '18.05.00.000',
    maximum_version => undef,
    version         => $VERSION,
    description     => 'This plugin will help the developer to navigate into the '
      . ' different modules of Koha with as less click as possible.'
      . 'Also it will generate the sample data you need when you need it',
};

sub new {
    my ( $class, $args ) = @_;

    $args->{'metadata'} = $metadata;
    $args->{'metadata'}->{'class'} = $class;

    my $self = $class->SUPER::new($args);

    return $self;
}

sub opac_js {
    my ( $self ) = @_;

    return q|
    |;
}


sub intranet_head {
    my ( $self ) = @_;

    return q|
        <style>
          .ui-autocomplete {
            position: absolute;
            z-index: 1510 !important;
          }
          #dev_helper_modal .ui-autocomplete-input {
            z-index: 1511;
            position: relative;
          }

          #dev_helper_modal #sentence > input {
            text-align: center;
          }

          .modal-dialog{ // Maybe we need that in Koha code
              overflow-y: initial !important
          }
          .modal-body{
              height: 250px;
              overflow-y: auto;
          }
        </style>
    |;
}

sub intranet_js {
    my ( $self ) = @_;

    return q|
        <script src="/api/v1/contrib/jump/static/static_files/jump.js"></script>
    |;
}

#sub configure {
#    my ( $self, $args ) = @_;
#
#    # Add the shortcut configuration here
#
#}

sub install() {
    my ( $self, $args ) = @_;

    my $table = $self->get_qualified_table_name('mytable');

}

sub upgrade {
    my ( $self, $args ) = @_;

    my $dt = dt_from_string();
    $self->store_data( { last_upgraded => $dt->ymd('-') . ' ' . $dt->hms(':') } );

    return 1;
}

sub uninstall() {
    my ( $self, $args ) = @_;

    my $table = $self->get_qualified_table_name('mytable');

    return C4::Context->dbh->do("DROP TABLE IF EXISTS $table");
}

## API methods
# If your plugin implements API routes, then the 'api_routes' method needs
# to be implemented, returning valid OpenAPI 2.0 paths serialized as a hashref.
# It is a good practice to actually write OpenAPI 2.0 path specs in JSON on the
# plugin and read it here. This allows to use the spec for mainline Koha later,
# thus making this a good prototyping tool.

sub api_routes {
    my ( $self, $args ) = @_;

    my $spec_str = $self->mbf_read('openapi.json');
    my $spec     = decode_json($spec_str);

    return $spec;
}

sub api_namespace {
    my ( $self ) = @_;

    return 'jump';
}

sub static_routes {
    my ( $self, $args ) = @_;

    my $spec_str = $self->mbf_read('staticapi.json');
    my $spec     = decode_json($spec_str);

    return $spec;
}

1;
